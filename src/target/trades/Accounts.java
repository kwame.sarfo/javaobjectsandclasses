package target.trades;

import java.util.ArrayList;
import java.util.List;

public class Accounts {
    private  float totalValueOfAllTrades;
    private List<Trade> trades;
    public Accounts()
    {
        this.trades = new ArrayList<>();
        this.totalValueOfAllTrades = 0;
    }

    public  void setTotalValueOfAllTrades(String id, String symbol, int quantity, float price) {
        this.trades.add(new BondTrade(id, symbol, quantity, price, 0.025f));
        this.totalValueOfAllTrades += quantity * price;
    }

    public float getTotalValueOfAllTrades() {
        return totalValueOfAllTrades;
    }

    public List<Trade> getTrades() {
        return trades;
    }


}
