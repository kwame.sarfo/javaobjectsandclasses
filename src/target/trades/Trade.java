package target.trades;

import target.Exceptions.TradeIDException;

import java.time.LocalDate;
import java.time.LocalTime;
import java.util.Calendar;


public abstract class Trade {
    protected String id;
    protected String symbol;
    protected int quantity;
    public float price;
    protected LocalDate tradeDate;
    protected int tradeHour;


    public Trade(String id, String symbol, int quantity, float price)
    {
        this.id = id;
        this.symbol = symbol;
        this.tradeDate = LocalDate.now();
        this.tradeHour = LocalTime.now().getHour();
        this.quantity = quantity;
        this.price = price;
    }

    public void setPrice(float price)
    {
        if(price > 0.0)
        {
            this.price = price;
        }
    }

    public int getQuantity() {
        return quantity;
    }

    public void setId(String id) throws TradeIDException {
        if(this.id == id)
        {
            throw new TradeIDException();
        }
        else {
            this.id = id;
        }
    }

    public float getPrice() {
        return this.price;
    }

    public LocalDate getTradeDate() {
        return this.tradeDate;
    }

    public int getHourOfTrade() {
        return this.tradeHour;
    }

    public void setTradeDate(LocalDate tradeDate) {
        this.tradeDate = tradeDate;
    }

    public void setTradeHour(int tradeHour) {
        this.tradeHour = tradeHour;
    }

//    @Override
//    public String toString() {
//        return "Trade{" +
//                "id='" + id + '\'' +
//                ", symbol='" + symbol + '\'' +
//                ", quantity=" + quantity +
//                ", price=" + price +
//                ", tradeDate=" + tradeDate +
//                ", tradeHour=" + tradeHour +
//                '}';
//    }

    public abstract float calcDividend();

}
