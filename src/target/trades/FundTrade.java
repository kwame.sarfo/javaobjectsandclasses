package target.trades;

public class FundTrade extends Trade {
    private float dividend;

    public FundTrade(String id, String symbol, int quantity, float price, float dividendPercent)
    {
        super(id, symbol, quantity, price);
        this.dividend = dividendPercent;

    }

    public float calcDividend()
    {
        return this.dividend * this.price;
    }
}
