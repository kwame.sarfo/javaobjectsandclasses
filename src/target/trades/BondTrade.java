package target.trades;

public class BondTrade extends Trade{
    private float dividend;

    public BondTrade(String id, String symbol, int quantity, float price, float dividend)
    {
        super(id, symbol, quantity, price);
        this.dividend = dividend;
    }

    public float calcDividend()
    {
        return this.dividend;
    }
}
