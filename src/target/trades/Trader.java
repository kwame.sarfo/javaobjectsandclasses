package target.trades;

import java.util.List;

public class Trader extends Accounts{
    private String name;
    private Accounts traderAccount;

    public Trader(String name)
    {
        this.name = name;
        traderAccount = new Accounts();
    }


    public void addTrade(String id, String symbol, int quantity, float price)
    {
        this.traderAccount.setTotalValueOfAllTrades(id, symbol, quantity, price);
    }

    public float getTraderAccount() {
        return traderAccount.getTotalValueOfAllTrades();
    }

    public List<Trade> getTrades() {
        return this.traderAccount.getTrades();
    }


    public String getName() {
        return name;
    }
}
