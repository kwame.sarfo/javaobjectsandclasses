package target.Exceptions;

public class BronzeTimeRestriction extends Exception{

    public BronzeTimeRestriction()
    {
        super("You can't trade before 10AM as a Bronze client");
    }
}
