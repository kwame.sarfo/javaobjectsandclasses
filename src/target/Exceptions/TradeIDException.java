package target.Exceptions;

public class TradeIDException extends Exception{

    public TradeIDException()
    {
        super("A duplicate ID was used.");
    }
}
