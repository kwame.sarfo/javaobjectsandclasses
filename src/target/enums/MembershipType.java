package target.enums;

public enum MembershipType {
    NONE(0),
    BRONZE(5),
    SILVER(10),
    GOLD(20);

    private int dailyLimit;

    MembershipType(int dailyLimit){
        this.dailyLimit = dailyLimit;
    }

    public int getDailyLimit() {
        return dailyLimit;
    }
}
