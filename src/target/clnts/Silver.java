package target.clnts;

public class Silver extends MembershipType {

    public Silver()
    {
        this.clientStatus = target.enums.MembershipType.SILVER;
    }

    public target.enums.MembershipType getMembership()
    {
        return this.clientStatus;
    }

}
