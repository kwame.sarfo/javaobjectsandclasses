package target.clnts;

public class Bronze extends MembershipType {

    public Bronze()
    {
        this.clientStatus = target.enums.MembershipType.BRONZE;
        this.timeRestriction = 10;

    }

    public target.enums.MembershipType getMembership()
    {
        return this.clientStatus;
    }

}
