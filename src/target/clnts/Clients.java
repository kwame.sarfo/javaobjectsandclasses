package target.clnts;

import target.trades.Accounts;
import target.trades.Trade;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

public class Clients {
    private String firstName;
    private String lastName;
    private int points;
    private MembershipType memberType;
    private int totalDailyTradeNumber;
    private int totalDailyTradeValue;
    private int maxDailyTradeNumber;
    private int maxDailyTradeValue;
    private LocalDate oldDate;
    protected List<Trade> allTrades;


    public Clients(String firstName, String lastName)
    {
        this.firstName = firstName;
        this.lastName = lastName;
        this.maxDailyTradeValue = 10000;
        this.maxDailyTradeNumber = target.enums.MembershipType.BRONZE.getDailyLimit();
        this.oldDate = LocalDate.now();
        this.allTrades = new ArrayList<Trade>();
    }


    public boolean canTrade(LocalDate tradeDate, int tradeHour)
    {

        if(this.memberType != null && this.memberType.timeRestriction != 0 && tradeHour < this.memberType.timeRestriction)
        {
            return false;

        }
        if(this.oldDate.getDayOfMonth() == tradeDate.getDayOfMonth() && this.totalDailyTradeNumber < this.maxDailyTradeNumber && this.totalDailyTradeValue < this.maxDailyTradeValue)
        {
            return true;
        }
        else if(tradeDate.isAfter(this.oldDate))
        {
            this.newDay();
            this.oldDate = tradeDate;
            return this.canTrade(tradeDate, tradeHour);
        }
        else{
            return false;
        }
    }

    public void newDay()
    {
        this.totalDailyTradeValue = 0;
        this.totalDailyTradeNumber = 0;
    }


    public String addTrade(Trade newTrade)
    {
        if(this.canTrade(newTrade.getTradeDate(), newTrade.getHourOfTrade()))
        {
//            Accounts.setTotalValueOfAllTrades(newTrade.getQuantity(), newTrade.getPrice());
            this.allTrades.add(newTrade);
            this.points++;
            upgradeMembership();
            this.totalDailyTradeNumber++;
            this.totalDailyTradeValue+=newTrade.price;
            return "Trade Added Successfully.";
        }
        else
        {
            return "You are not eligible to trade.";
        }

    }

    public int getPoints()
    {
        return this.points;
    }

    public target.enums.MembershipType checkMembership()
    {
        if(this.memberType == null)
        {
            return target.enums.MembershipType.NONE;
        }
        else
        {
            return this.memberType.clientStatus;
        }
    }

    public void upgradeMembership()
    {
        if(this.points <10 && this.points>0)
        {
            this.memberType = new Bronze();
            this.maxDailyTradeNumber = target.enums.MembershipType.BRONZE.getDailyLimit();
        }
        else if(this.points<19 && this.points>=10)
        {
            this.memberType = new Silver();
            this.maxDailyTradeNumber = target.enums.MembershipType.SILVER.getDailyLimit();
        }
        else if(this.points >= 20){
            this.memberType = new Gold();
            this.maxDailyTradeNumber = target.enums.MembershipType.GOLD.getDailyLimit();
        }
    }

    public int checkTradesRemaining()
    {
        return this.maxDailyTradeNumber - this.totalDailyTradeNumber;
    }

    public List<Trade> getAllTrades() {
        return allTrades;
    }
}
