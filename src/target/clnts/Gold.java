package target.clnts;

public class Gold extends MembershipType {

    public Gold()
    {
        this.clientStatus = target.enums.MembershipType.GOLD;

    }

    public target.enums.MembershipType getMembership()
    {
        return this.clientStatus;
    }
}
