package target.Tests;

import org.junit.jupiter.api.Test;
import target.trades.BondTrade;
import target.trades.FundTrade;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;

public class TradeTests {

    @Test
    public void checkBondFundDividendSameAsParameter(){
        BondTrade newBondTrade = new BondTrade("uid32", "AAPL", 3, 3.2f, 0.125f);
        assertEquals(0.125f, newBondTrade.calcDividend());
    }

    @Test
    public void checkFundTradeDividendSuppliedIsPercentageOfPrice(){
        FundTrade newFundTrade = new FundTrade("uid32", "AAPL", 3, 3.2f, 0.125f);
        assertEquals(0.125f*newFundTrade.price, newFundTrade.calcDividend());
    }

    @Test
    public void duplicatedIDThrowsEx()
    {
        BondTrade newBondTrade = new BondTrade("uid32", "AAPL", 3, 3.2f, 0.125f);

        Throwable ex = assertThrows(Throwable.class, ()-> newBondTrade.setId("uid32"));
        assertEquals("A duplicate ID was used.", ex.getMessage());
    }
}
