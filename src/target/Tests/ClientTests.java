package target.Tests;

import target.clnts.Clients;
import org.junit.jupiter.api.Test;
import target.enums.MembershipType;
import target.trades.Accounts;
import target.trades.BondTrade;
import target.trades.Trade;

import java.time.LocalDate;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class ClientTests {
    @Test
    public void newClientPointsZero()
    {
        Clients newClient = new Clients("John", "Doe");
        assertEquals(0, newClient.getPoints());
    }

    @Test
    public void newClientHasNoMembership()
    {
        Clients newClient = new Clients("John", "Doe");
        assertEquals(MembershipType.NONE, newClient.checkMembership());
    }

    @Test
    public void clientPointIncreases()
    {
        Clients newClient = new Clients("John", "Doe");
        assertEquals(0, newClient.getPoints());
        BondTrade newTrade = new BondTrade("uid32", "AAPL", 3, 3.2f, 0.125f);
        for(int iter = 1; iter < 5; iter++)
        {
            newClient.addTrade(newTrade);
        }

        assertEquals(4, newClient.getPoints());
        assertEquals(4, newClient.getAllTrades().size());

    }

    @Test
    public void clientHasBronzeMembershipAfterFourTrades()
    {
        Clients newClient = new Clients("John", "Doe");
        assertEquals(0, newClient.getPoints());
        for(int iter = 1; iter < 5; iter++)
        {
            newClient.addTrade(new BondTrade("uid32", "AAPL", 3, 3.2f, 0.125f));
        }
        assertEquals(4, newClient.getPoints());
        assertEquals(MembershipType.BRONZE, newClient.checkMembership());
    }

    @Test
    public void bronzeClientRemainingTradesAfterFourSameDay()
    {
        Clients newClient = new Clients("John", "Doe");
        assertEquals(0, newClient.getPoints());
        for(int iter = 1; iter < 5; iter++)
        {
            newClient.addTrade(new BondTrade("uid32", "AAPL", 3, 3.2f, 0.125f));
        }
        assertEquals(4, newClient.getPoints());
        assertEquals(MembershipType.BRONZE, newClient.checkMembership());
        assertEquals(1, newClient.checkTradesRemaining());
    }

    @Test
    public void bronzeClientCantTradeBefore10AM()
    {
        Clients newClient = new Clients("John", "Doe");
        BondTrade newTrade = new BondTrade("uid32", "AAPL", 3, 3.2f, 0.125f);
        newClient.addTrade(newTrade);


        BondTrade newTrade1 = new BondTrade("uid32", "AAPL", 3, 3.2f, 0.125f);
        newTrade1.setTradeHour(9);
        assertEquals("You are not eligible to trade.", newClient.addTrade(newTrade1));

    }

    @Test
    public void clientHasSilverMembershipAfterTenTradesOverDifferentDays()
    {
        Clients newClient = new Clients("John", "Doe");
        assertEquals(0, newClient.getPoints());
        BondTrade newTrade = new BondTrade("uid32", "AAPL", 3, 3.2f, 0.125f);
        for(int iter = 1; iter < 6; iter++)
        {
            newClient.addTrade(newTrade);
        }
        assertEquals(5, newClient.getPoints());
        assertEquals(MembershipType.BRONZE, newClient.checkMembership());



        BondTrade newTrade1 = new BondTrade("uid32", "AAPL", 3, 3.2f, 0.125f);
        newTrade1.setTradeDate(LocalDate.now().plusDays(1));


        for(int iter = 1; iter < 7; iter++)
        {
            newClient.addTrade(newTrade1);
        }
        assertEquals(11, newClient.getPoints());
        assertEquals(MembershipType.SILVER, newClient.checkMembership());

    }

    @Test
    public void silverClientRemainingTradesAfterTenOverDiffDays()
    {
        Clients newClient = new Clients("John", "Doe");
        assertEquals(0, newClient.getPoints());
        BondTrade newTrade = new BondTrade("uid32", "AAPL", 3, 3.2f, 0.125f);
        for(int iter = 1; iter < 6; iter++)
        {
            newClient.addTrade(newTrade);
        }
        assertEquals(5, newClient.getPoints());
        assertEquals(MembershipType.BRONZE, newClient.checkMembership());

        BondTrade newTrade1 = new BondTrade("uid32", "AAPL", 3, 3.2f, 0.125f);
        newTrade1.setTradeDate(LocalDate.now().plusDays(1));


        for(int iter = 1; iter < 7; iter++)
        {
            newClient.addTrade(newTrade1);
        }

        assertEquals(11, newClient.getPoints());
        assertEquals(MembershipType.SILVER, newClient.checkMembership());
        assertEquals(4, newClient.checkTradesRemaining());
    }

    @Test
    public void clientHasGoldMembershipAfterTwentyTradesOverDiffDays()
    {
        Clients newClient = new Clients("John", "Doe");
        assertEquals(0, newClient.getPoints());
        Trade newTrade = new BondTrade("uid32", "AAPL", 3, 3.2f, 0.125f);

        for(int iter = 1; iter < 6; iter++)
        {
            newClient.addTrade(newTrade);
        }
        assertEquals(5, newClient.getPoints());
        assertEquals(MembershipType.BRONZE, newClient.checkMembership());


        Trade newTrade1 = new BondTrade("uid32", "AAPL", 3, 3.2f, 0.125f);
        newTrade1.setTradeDate(LocalDate.now().plusDays(1));

        for(int iter = 1; iter < 11; iter++)
        {
            newClient.addTrade(newTrade1);
        }
        assertEquals(15, newClient.getPoints());

        BondTrade newTrade2 = new BondTrade("uid32", "AAPL", 3, 3.2f, 0.125f);
        newTrade2.setTradeDate(LocalDate.now().plusDays(2));

        for(int iter = 1; iter < 7; iter++)
        {
            newClient.addTrade(newTrade2);
        }
        assertEquals(21, newClient.getPoints());
        assertEquals(MembershipType.GOLD, newClient.checkMembership());
    }

    @Test
    public void goldClientRemainingTradesAfterTwentyOverDiffDays()
    {
        Clients newClient = new Clients("John", "Doe");
        assertEquals(0, newClient.getPoints());
        Trade newTrade1 = new BondTrade("uid32", "AAPL", 3, 3.2f, 0.125f);

        for(int iter = 1; iter < 6; iter++)
        {
            newClient.addTrade(newTrade1);
        }
        assertEquals(5, newClient.getPoints());
        assertEquals(MembershipType.BRONZE, newClient.checkMembership());

        Trade newTrade2 = new BondTrade("uid32", "AAPL", 3, 3.2f, 0.125f);
        newTrade2.setTradeDate(LocalDate.now().plusDays(1));

        for(int iter = 1; iter < 11; iter++)
        {
            newClient.addTrade(newTrade2);
        }
        assertEquals(15, newClient.getPoints());

        Trade newTrade3 = new BondTrade("uid32", "AAPL", 3, 3.2f, 0.125f);
        newTrade3.setTradeDate(LocalDate.now().plusDays(2));

        for(int iter = 1; iter < 7; iter++)
        {
            newClient.addTrade(newTrade3);
        }
        assertEquals(21, newClient.getPoints());
        assertEquals(14, newClient.checkTradesRemaining());


    }

    @Test
    public void clientCantTradeAfterDailyTradeExhausted()
    {
        Clients newClient = new Clients("John", "Doe");
        assertEquals(0, newClient.getPoints());
        for(int iter = 1; iter < 9; iter++)
        {
            newClient.addTrade(new BondTrade("uid32", "AAPL", 3, 3.2f, 0.125f));
        }
        assertEquals(5, newClient.getPoints());
        assertEquals(MembershipType.BRONZE, newClient.checkMembership());
        assertEquals(0, newClient.checkTradesRemaining());
    }


}
