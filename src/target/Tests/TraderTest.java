package target.Tests;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import target.trades.Trader;

public class TraderTest {

    @Test
    public void traderAddsNewTrade()
    {
        Trader trader = new Trader("John");
        trader.addTrade("uid1", "USD", 3, 12.0f);
        Assertions.assertEquals(36.0, trader.getTraderAccount());
        StringBuilder sb = new StringBuilder();
        sb.append("----------------------")
                .append('\n')
                .append("Account Holder: ")
                .append(trader.getName())
                .append('\n')
                .append("Account Balance: ")
                .append(trader.getTraderAccount())
                .append('\n')
                .append("----------------------");
        System.out.println(sb.toString());
    }

    @Test
    public void traderAddsNewTwoTrades()
    {
        Trader trader = new Trader("John");
        trader.addTrade("uid1", "USD", 3, 12.0f);
        Assertions.assertEquals(36.0, trader.getTraderAccount());
        trader.addTrade("uid1", "USD", 3, 12.0f);
        Assertions.assertEquals(72.0, trader.getTraderAccount());
        StringBuilder sb = new StringBuilder();
        sb.append("----------------------")
                .append('\n')
                .append("Account Holder: ")
                .append(trader.getName())
                .append('\n')
                .append("Account Balance: ")
                .append(trader.getTraderAccount())
                .append('\n')
                .append("----------------------");
        System.out.println(sb.toString());
        System.out.println(trader.getTrades());

    }
}
